## 聚合支付sdk 
 
 [![``0.0.3-release``](https://img.shields.io/maven-metadata/v?metadataUrl=https%3A%2F%2Fbintray.com%2Fapi%2Fui%2Fdownload%2Foldchen%2Fmaven%2Fcom%2Foldchen%2Fmix_pay_test%2Fmaven-metadata.xml)](https://bintray.com/beta/#/oldchen/maven/mix_pay_test?tab=overview) 


### 一、添加依赖

在android项目中的app目录下，找到``build.gradle``文件，在dependencies中添加如下内容：

```
dependencies {
    ```
    implementation 'com.oldchen:mix_pay_test:0.0.3-release'
    ```
}
```

添加依赖时，请使用最新版本


### 二、使用

#### 1.支付宝支付


```
    /**
     * @param orderInfo 订单信息
     * @param isSandBox 是否为沙箱模式
     */
    private void aliPay(String orderInfo, boolean isSandBox) {
        if (isSandBox) {
            //开启沙箱支付
            EnvUtils.setEnv(EnvUtils.EnvEnum.SANDBOX);
        }
        //实例化支付宝支付策略
        AliPay aliPay = new AliPay();
        //构造支付宝订单实体。一般都是由服务端直接返回。
        AlipayInfoImpli alipayInfoImpli = new AlipayInfoImpli();
        alipayInfoImpli.setOrderInfo(orderInfo);
        //策略场景类调起支付方法开始支付，以及接收回调。
        AllPay.pay(aliPay, this, alipayInfoImpli, new IPayCallback() {
            @Override
            public void success() {
                showToast("支付成功");
            }

            @Override
            public void failed(int code, String message) {
                showToast("支付失败" + "  code：" + code + "   message：" + message);
            }

            @Override
            public void cancel() {
                showToast("支付取消");
            }
        });
    }
```


#### 2.银联支付（云闪付）


```
    /**
     * @param tn   订单号
     * @param mode 模式
     */
    private void unionPay(String tn, Mode mode) {
        //实例化银联支付策略
        UnionPay unionPay = new UnionPay();
        //构造银联订单实体。一般都是由服务端直接返回。测试时可以用Mode.TEST,发布时用Mode.RELEASE。
        UnionPayInfoImpli unionPayInfoImpli = new UnionPayInfoImpli();
        unionPayInfoImpli.setTn(tn);
        unionPayInfoImpli.setMode(mode);
        //策略场景类调起支付方法开始支付，以及接收回调。
        AllPay.pay(unionPay, this, unionPayInfoImpli, new IPayCallback() {
            @Override
            public void success() {
                showToast("支付成功");
            }

            @Override
            public void failed(int code, String message) {
                showToast("支付失败");
            }

            @Override
            public void cancel() {
                showToast("支付取消");
            }
        });
    }
```


#### 微信支付

微信支付需要多一些配置，在android项目中，找到 ``app -> src -> main`` 目录下的 ``AndroidManifest.xml`` 文件，添加下面的内容


```
    <application···>
    ···
        <activity-alias
            android:name=".wxapi.WXPayEntryActivity"
            android:exported="true"
            android:targetActivity="com.example.polymerizationpay.activity.WXPayActivity" />
    ···
    </application>

```
因为微信sdk使用，需要创建一个 ``WXPayEntryActivity``,而在聚合支付sdk中，这个activity已经被创建了

然后就是微信支付的使用了

```
    private void weChatPay() {
        //实例化微信支付策略
        WXPay wxPay = WXPay.getInstance();
        //构造微信订单实体。一般都是由服务端直接返回。
        WXPayInfoImpli wxPayInfoImpli = new WXPayInfoImpli();
        //时间戳
        wxPayInfoImpli.setTimestamp("");
        //签名
        wxPayInfoImpli.setSign("");
        //预支付交易会话ID
        wxPayInfoImpli.setPrepayId("");
        //商户号
        wxPayInfoImpli.setPartnerid("");
        //appid
        wxPayInfoImpli.setAppid("");
        //随机字符串
        wxPayInfoImpli.setNonceStr("");
        //扩展字段
        wxPayInfoImpli.setPackageValue("");
        //策略场景类调起支付方法开始支付，以及接收回调。
        AllPay.pay(wxPay, this, wxPayInfoImpli, new IPayCallback() {
            @Override
            public void success() {
                showToast("支付成功");
            }

            @Override
            public void failed(int code, String message) {
                showToast("支付失败" + "  code：" + code + "   message：" + message);
            }

            @Override
            public void cancel() {
                showToast("支付取消");
            }
        });
    }
```


⚠️注意：微信支付需要保证，app项目的 **应用包名** 以及 **应用签名** 与微信开放平台上一致。

获取 **应用签名** 的方式是通过微信提供的签名工具来获取：https://open.weixin.qq.com/zh_CN/htmledition/res/dev/download/sdk/Gen_Signature_Android.apk

（debug版与release版的签名不一致，也会导致微信支付失败。电脑A上编译的debug版与电脑B上的debug版签名也会不一致。建议使用固定的jks文件对apk进行签名）

