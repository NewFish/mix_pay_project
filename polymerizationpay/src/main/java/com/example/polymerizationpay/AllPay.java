/*
******************************* Copyright (c)*********************************\
**
**                 (c) Copyright 2017, King, china
**                          All Rights Reserved
**                                
**                              By(King)
**                         
**------------------------------------------------------------------------------
*/
package com.example.polymerizationpay;

import android.app.Activity;

import com.example.polymerizationpay.base.IPayInfo;
import com.example.polymerizationpay.base.IPayStrategy;
import com.example.polymerizationpay.callback.IPayCallback;

public class AllPay {
    public static <T extends IPayInfo> void pay(IPayStrategy<T> payWay, Activity mActivity, T payinfo, IPayCallback callback){
        payWay.pay(mActivity, payinfo, callback);
    }
}
