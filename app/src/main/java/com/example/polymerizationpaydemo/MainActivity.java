package com.example.polymerizationpaydemo;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.alipay.sdk.app.EnvUtils;
import com.example.polymerizationpay.AllPay;
import com.example.polymerizationpay.alipay.AliPay;
import com.example.polymerizationpay.alipay.AlipayInfoImpli;
import com.example.polymerizationpay.callback.IPayCallback;
import com.example.polymerizationpay.unionpay.Mode;
import com.example.polymerizationpay.unionpay.UnionPay;
import com.example.polymerizationpay.unionpay.UnionPayInfoImpli;
import com.example.polymerizationpay.wxpay.WXPay;
import com.example.polymerizationpay.wxpay.WXPayInfoImpli;
import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.bt_ali).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showEditDialog("请输入支付宝订单信息", PayType.aliPay);
            }
        });

        findViewById(R.id.bt_union).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showEditDialog("请输入云闪付账单号", PayType.unionPay);
            }
        });

        findViewById(R.id.bt_we).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showEditDialog("请输入微信订单信息(参考如下JSON格式)", PayType.wePay);
            }
        });
    }

    /**
     * @param tn   订单号
     * @param mode 模式
     */
    private void unionPay(String tn, Mode mode) {
        //实例化银联支付策略
        UnionPay unionPay = new UnionPay();
        //构造银联订单实体。一般都是由服务端直接返回。测试时可以用Mode.TEST,发布时用Mode.RELEASE。
        UnionPayInfoImpli unionPayInfoImpli = new UnionPayInfoImpli();
        unionPayInfoImpli.setTn(tn);
        unionPayInfoImpli.setMode(mode);
        //策略场景类调起支付方法开始支付，以及接收回调。
        AllPay.pay(unionPay, this, unionPayInfoImpli, new IPayCallback() {
            @Override
            public void success() {
                showToast("支付成功");
            }

            @Override
            public void failed(int code, String message) {
                showToast("支付失败");
            }

            @Override
            public void cancel() {
                showToast("支付取消");
            }
        });
    }

    private void weChatPay(String orderInfo) {
        //实例化微信支付策略
        WXPay wxPay = WXPay.getInstance();
        //构造微信订单实体。一般都是由服务端直接返回。
        WXPayInfoImpli wxPayInfoImpli;
        try {
            wxPayInfoImpli = new Gson().fromJson(orderInfo, WXPayInfoImpli.class);
            if(TextUtils.isEmpty(wxPayInfoImpli.getPackageValue())){
                wxPayInfoImpli.setPackageValue("Sign=WXPay");
            }
            //策略场景类调起支付方法开始支付，以及接收回调。
            AllPay.pay(wxPay, this, wxPayInfoImpli, new IPayCallback() {
                @Override
                public void success() {
                    showToast("支付成功");
                }

                @Override
                public void failed(int code, String message) {
                    showToast("支付失败" + "  code：" + code + "   message：" + message);
                }

                @Override
                public void cancel() {
                    showToast("支付取消");
                }
            });
        }catch (Exception e){
            showToast("订单解析失败:" + e.getMessage());
        }

    }

    /**
     * @param orderInfo 订单信息
     * @param isSandBox 是否为沙箱模式
     */
    private void aliPay(String orderInfo, boolean isSandBox) {
        if (isSandBox) {
            //开启沙箱支付
            EnvUtils.setEnv(EnvUtils.EnvEnum.SANDBOX);
        }
        //实例化支付宝支付策略
        AliPay aliPay = new AliPay();
        //构造支付宝订单实体。一般都是由服务端直接返回。
        AlipayInfoImpli alipayInfoImpli = new AlipayInfoImpli();
        alipayInfoImpli.setOrderInfo(orderInfo);
        //策略场景类调起支付方法开始支付，以及接收回调。
        AllPay.pay(aliPay, this, alipayInfoImpli, new IPayCallback() {
            @Override
            public void success() {
                showToast("支付成功");
            }

            @Override
            public void failed(int code, String message) {
                showToast("支付失败" + "  code：" + code + "   message：" + message);
            }

            @Override
            public void cancel() {
                showToast("支付取消");
            }
        });
    }

    private void showToast(String context) {
        Toast.makeText(this, context, Toast.LENGTH_LONG).show();
    }

    private void showEditDialog(String title, final PayType type) {
        final EditText edit = new EditText(this);
        AlertDialog.Builder editDialog = new AlertDialog.Builder(this);
        editDialog.setTitle(title);
        switch (type){
            case wePay:
                editDialog.setIcon(R.drawable.ic_wechat_pay);
                edit.setText("{\"sign\":\"ECE311C3DF76E009E6F37F05C350625F\",\"timestamp\":\"1474886901\",\"partnerid\":\"1391669502\",\"package\":\"Sign=WXPay\",\"appid\":\"wx46a24ab145becbde\",\"nonceStr\":\"0531a4a42fa846fe8a7563847cd24c2a\",\"prepayId\":\"wx20160926184820acbd9357100240402425\"}");
                break;
            case aliPay:
                editDialog.setIcon(R.drawable.ic_ali_pay);
                break;
            case unionPay:
                editDialog.setIcon(R.drawable.ic_union_pay);
                break;
        }
        editDialog.setView(edit);
        editDialog.setPositiveButton("确认"
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (type){
                            case wePay:
                                weChatPay(edit.getText().toString());
                                break;
                            case aliPay:
                                aliPay(edit.getText().toString(), true);
                                break;
                            case unionPay:
                                unionPay(edit.getText().toString(), Mode.RELEASE);
                                break;
                        }
                    }
                }).create().show();
    }

    enum PayType{
        wePay,
        aliPay,
        unionPay
    }
}
